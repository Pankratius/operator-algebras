These are my (probably extremely erroneous and also very provisional) lecture notes for the course [_Thèmes choisis en algèbres d'opérateurs_][1] taught by Prof. C. Debord at the Sorbonne Université.
A compiled version can be found [here][2].


[1]: https://master-math-fonda.imj-prg.fr/2021-22/fiches/Debord-intro.html
[2]: https://pankratius.gitlab.io/operator-algebras/operator-algebras.pdf
