\section{Banach Algebras and Spectra}
\begin{nonumnotation}
	Let $A$ be a unitary $\kk$-algebra.
	We denote its unit element by $\une$ and regard an element $\lambda \in \kk$ as an element of $A$ via the map $\lambda \mapsto \lambda \cdot \une$.
	For the group of units of $A$, we write.
	\[
		\unit{A}
		\defined 
		\lset
		x\in A
		\ssp
		\text{$x$ is invertible}
		\rset.
	\]
	Note that this is a different notation than in the lecture, where we used $A^{-1}$ for that, but this is too confusing for me.
	\glsadd{units}
\end{nonumnotation}

\subsection{Spectra for general algebras}

\begin{defn}
	Let $A$ be a unitary $\kk$-algebra, and $x\in A$.
	The \emph{spectrum of $x$} is defined as the subset 
	\[
		\spa_A(x) 
		\defined 
		\lset
		\lambda \in \kk
		\ssp
		\left(\lambda - x\right)
		\notin 
		\unit{A}
		\rset
		\sse 
		\kk.
	\]
	\glsadd{spectrum}
\end{defn}

\begin{lem}
	\label{banach:facile}
	Let $A$ be a unitary $\kk$-algebra.
	\begin{enumerate}
		\item
			\label{banach:facile:commuting}
			Let $a,b \in A$ be commuting elements.
			Then $ab \in \units{A}$ if and only if $a,b\in \units{A}$.
		\item
			\label{banach:facile:ps}
			Let now $a,b \in A$ be arbitrary elements.
			Then $1-ab$ is invertible if and only if $1-ba$ is invertible.
		\item
			\label{banach:facile:polynomial}
			Let $a\in A$ and $p\in \kk[t]$ a non-zero polynomial.
			Then $p(a)$ is invertible in $A$ if and only if $\spa(a)$ does not contain a root of $p$.
	\end{enumerate}
\end{lem}

\begin{proof}
	Part \ref{banach:facile:commuting} is a simple computation.
	For \ref{banach:facile:ps}, we denote the right-sided inverse of $(1-ab)$ by $x$.
	We then have 
	\begin{align*}
		(1-ab) x &= 1 \implies abx = x-1,
		\intertext{which implies}
		(1-ba)(1+bxa)
			 &=
			 1 + bxa - ba - b(abx)a
			\\
			 &=
			 1 + bxa - ba - b(x-1)a
			 \\
			 &=
			 1;
	\end{align*}
	we can do the same calculation for a left-sided inverse of $1-ab$.
	Finally for \ref{banach:facile:polynomial}, we write $p = \alpha \cdot \prod (t - \lambda_j)$ for some $\lambda_j \in \kk$.
	Then $p(a)$ is invertible if and only if $x-\lambda_j$ is invertible for all $j$ (here we use that $(x-\lambda_i) (x-\lambda_j) = (x-\lambda_j)(x-\lambda_i)$ holds for all $i,j$).
\end{proof}

\begin{construction}
	Let $A$ be a (possibly non-unital) algebra over $\kk$.
	The \emph{unitalization} of $A$ is the $\kk$-algebra $\wtilde{A}$ given by the vector space $\widetilde{A} \defined A \oplus \kk$ and the multiplication defined by 
	\[
		(a,\lambda)
		\cdot 
		(b,\mu)
		\defined 
		(ab + \lambda b + \mu a, \lambda \mu).
	\]
	This is a unital algebra, with the unit element given by the element $(0,\une_{\kk})$.
	\glsadd{unitalization}
\end{construction}

\begin{prop}
	Let $A$ be a $\kk$-algebra.
	Then the map
	\[
		\iota\mc 
		A
		\to 
		\widetilde{A},
		~
		a
		\mapsto 
		(a,0)
	\]
	is an injective homomorphism of $\kk$-algebras; the image of $A$ under $\iota$ in $\widetilde{A}$ is an ideal of codimension $1$.
	The unitalization is unviveral in the following sense:
	Let $B$ be a unital algebra and $\pphi\mc A \to B$ a homomorphism of $\kk$-algebras.
	Then $\pphi$ extends uniquely to a homomorphism of $\kk$-algebras $\widetilde{\pphi}\mc \widetilde{A} \to B$ such that the diagram
	\[
	\begin{tikzcd}
		A
		\ar{r}[above]{\pphi}
		\ar[hookrightarrow]{d}[left]{\iota}
		&
		B
		\\
		\widetilde{A}
		\ar[dashed]{ur}[below right]{\exists !\widetilde{\pphi}}
		&
	\end{tikzcd}
\]
	commutes.
	In particular, $\widetilde{\pphi}(\une_{\widetilde{A}}) = \une_B$ holds.
\end{prop}

\begin{mlem}
	Let $A$ be a $\kk$-algebra.
	Then $A \cap \units{\widetilde{A}} = \emptyset$.
\end{mlem}

\begin{defn}
	Let $A$ be a $\kk$-algebra with unitalization $\widetilde{A}$.
	The \emph{spectrum} of an element $a\in A$ is defined as $\spa'_A(a) \defined \spa_{\wtilde{A}}(a)$.
\end{defn}

\begin{rem}
	If $A$ is a unital $\kk$-algebra, then $\widetilde{A} \cong A \times \kk$ holds as $\kk$-algebras.
	For every $a\in A$ it holds that $\spa_A'(a) = \spa_A(a) \cup \lset 0 \rset $.
	If $A$ is a non-unital $\kk$-algebra, we will drop the dash from the notation.
	We will do so in the unital case as well, when its clear from the context if we want to include zero or not.
\end{rem}


\begin{lem}
	\leavevmode
	\begin{enumerate}
		\item
			Let $A$ be a $\kk$-algebra.
			Then $\spa_{A}(xy) = \spa_A(yx)$ holds for all $x,y\in A$.
		\item
			Assume now that $A,B$ are unital $\kk$-algebras, and let $\pphi \mc A \to B$ be a morphism of algebras. Then 
			\[
				\spa_B(\pphi(x)) 
				\sse 
				\spa_A (x)
			\]
			holds in $\kk$.
		\end{enumerate}
\end{lem}

\begin{example}
	Let $\kk(t) \defined \fracfield(\kk[t])$ be the field of rational functions on $\kk$ in the variable $t$.
	For any $f \in \kk(t)$ we write $\poles(f) \sse \kk$ for the set of poles of $f$ in $\kk$.
	Let $S \sse \kk$ be a subset and write 
	\[
		\kk(t)_S \defined 
		\lset
		p \in \kk(t)
		\ssp
		\poles(p) \cap S = \emptyset
		\rset,
	\]
	i.e. the set of rational functions that don't have a pole in $S$.
	Then $\spa_{\kk(t)_S}(t) = S$: 
	Indeed, for every $\lambda \in \kk$, we have the (only tautoligal appearing) identity:
	\[
		\left(t-\lambda\right)^{-1}
		= 
		\frac{1}{t-\lambda}
	\]
	So $\lambda \in \spa_{\kk(t)_S}(t)$ if and onyl if $\lambda$ is not a pole of any of the polynomials of $\kk(t)_S$.
\end{example}

\begin{prop}
	Let $A$ be a $\kk$-algebra and $x\in A$.
	\begin{enumerate}
		\item
			The map 
			\[
				\kk(t)_{\spa_A(x)}
				\to 
				A,
				~
				f
				\mapsto
				f(x)
			\]
			is a well-defined homomorphism of $\kk$-algebras.
		\item
			For every $f \in \kk(t)_{\spa_A(x)}$ it holds that 
			\[
				f\left(\spa_A(x)\right)
				\sse
				\spa_A\left(f(x)\right)
				,
			\]
			with equality if and only if $\spa_A(x) = \emptyset$ and $f$ is constant.
		\end{enumerate}
\end{prop}

\subsection{Banach algebras}

\begin{defn}
	\leavevmode
	\begin{enumerate}
		\item
			A \emph{Banach space} $(E,\gnorm)$ is a $\kk$-vector space $E$ together with a norm $\gnorm \mc E \to \rr$ such that $E$ is complete with respect to $\gnorm$.
		\item
			A \emph{Banach algebra} $(A,\gnorm)$ is a $\kk$-algebra $A$ with a norm $\gnorm$ on the underlying $\kk$-vector space such that $(A,\gnorm)$ is a Banach space and the norm is \emph{submultiplicative}: for all $x,y\in A$, it holds that $\norm{xy} \leq \norm{x}\cdot \norm{y}$.
		\item
			A \emph{unital Banach algebra} is a Banach algebra $(A,\gnorm)$ such that the algebra $A$ is unital and $\norm{1} = 1$ is satisfied.
	\end{enumerate}
\end{defn}


\begin{example}
	Let $(E,\gnorm)$ be a Banach space.
	Then the algebra $\cl(E,E)$ of continous endomorphisms of $E$ is a Banach algebra, via the $\gnorm$-induced operator norm.
\end{example}

\begin{mrem}
	The reason we're demanding submultiplicativity is because it gives us that the multiplication map is continous: more precisely, let $(A,\gnorm)$ be an algebra with a norm, and endow $A\times A$ with the product topology.
	If the norm $\gnorm$ is submultiplicative, then the map
	\[
		A \times A
		\to 
		A,
		~
		(a,b)
		\mapsto
		ab
	\]
	is continous.
\end{mrem}

\begin{prop}
	Let $A$ be a nonzero, unital normed algebra with submultiplicative norm $\gnorm$.
	Then there exists a submultiplicative norm $\gnorm'$ on $A$ that is equivalent to $\gnorm$ such that $\norm{1}' = 1$.
\end{prop}

\begin{mdefn}
	Let $(E,\gnorm_E)$ be a normed vector space.
	A series $\sum_{j=0}^{\infty} a_j$ is called \emph{absolute convergent} if the series $\sum_{j=0}^{\infty} \norm{a_j}_E$ converges in $\rr$.
\end{mdefn}

\begin{mlem}
	Let $(E,\gnorm)$ be a normed vector space.
	Then the following are equivalent:
	\begin{enumerate}
		\item
			$(E,\gnorm)$ is a Banach space.
		\item
			Every absolute convergent series in $E$ is also convergent in $E$ (for the metric induced by $\gnorm_E$).
	\end{enumerate}
\end{mlem}

\begin{lem}
	\label{banach:gou}
	Let $A$ be a unital Banach algebra.
	\begin{enumerate}
		\item
			\label{banach:gou:ps}
			Let $a \in A$ be an element and suppose $\norm{a} < 1$.
			Then  $1-a$ is invertible in $A$ and it holds that 
		\[
			(1-a)^{-1}
			=
			\sum_{j=0}^{\infty} a_j.
		\]
		\item
			Assume now that $a\in \unit{A}$ is a unit, and let $b$ be an element of $A$ with
			\[
				\norm{a-b}
				<
				\frac{1}{\norm{a^{-1}}}.
			\]
			Then $b$ is invertible in $A$, and it holds that
			\[
				b^{-1}
				=
				\sum_{j=0}^{\infty}
				a^{-j+1}
				(a-b)^j.
			\]
		\item
			\label{banach:gou:is-open}
			The group $\units{A}$ is open in $A$.
	\end{enumerate}
\end{lem}

\begin{mdefn}
	Let $A$ be a normed $\cc$-algebra and $U\sse \cc$ an open subset.
	Let $\lambda_0 \in U$.
	A map of sets $f\mc U \to A$ is \emph{analytic} at $\lambda_0$ if there is a $\rho > 0$ and a sequence $(a_j)$ of elements of $A$ such that for every $\lambda \in U$ with $\abs{\lambda - \lambda_0} < \rho$, it holds that\footnote{In particular, this states that the sequence on the right-hand side converges. Omiting this part from statements seems standard, so we'll happily join.} 
	\[
		f(\lambda) 
		=
		\sum_{j=0}^{\infty}
		a_j
		\left(\lambda - \lambda_0\right)^{j}.
	\]
	The map $f$ is called an \emph{analytic function} if it is analytic at every $\lambda_0 \in U$.
\end{mdefn}

\begin{mprop}[Liouville's theorem]
	\label{banach:liouville}
	Let $A$ be a normed $\cc$-algebra and $U \sse \cc$ an open subset.
	\begin{enumerate}
		\item
			\label{banach:liouville:compostion}
			Let $f\mc U \to A$ be an analytic function, and $\pphi \in \cl(A,\cc)$ a continous linear functional on $A$.
			Then the composite $\pphi \circ f$ is an analytic function $U \to \cc$.
		\item
			\label{banach:liouville:statement}
			Let $f\mc \cc \to A$ be an analytic function.
			Then $f$ is either unbound or constant.
	\end{enumerate}
\end{mprop}

\begin{proof}
	The proof of \ref{banach:liouville:compostion} is a calculation (\coms which i should still add\come).
	For \ref{banach:liouville:statement}, we need the Hahn-Banach theorem in the form of \cref{app:fa:hb-separates}:
	Assume that $f$ is non-constant and let $x,y\in \cc$ be two different points with $f(x) \neq f(y)$.
	By the corollary, there is a linear functional $\Psi \mc A \to \cc$ that separates $f(x)$ and $f(y)$, i.e. $\Psi(f(x)) \neq \Psi(f(y))$ holds in $\cc$.
	So the function $\Psi \circ f\mc \cc \to \cc$ is non-constant.
	Since $\Psi$ is linear and $f$ is analytic, $\Psi \circ f$ is too. 
	By the classical version of Liouville's theorem we get that $\Psi \circ f$ is unbounded.
	Now $\norm{\Psi \circ f}_{\infty} \leq \norm{\Psi} \cdot \norm{f}_{\infty}$ holds, and since $\Psi$ is continous, we get that $f$ has to be unbounded.
\end{proof}

\begin{mdefn}
	Let $A$ be a unital $\kk$-algebra and $a\in A$ an element.
	The \emph{resolvent} of $a$ is the function
	\[
		\rslv_a
		\mc
		\kk \setminus \spa(a)
		\to
		A
		,~
		\lambda
		\mapsto
		(a-\lambda)^{-1}.
	\]
	\glsadd{resolvent}
\end{mdefn}
\begin{theorem}
	Let $A$ be a Banach algebra over $\cc$ and $a\in A$ an element.
	\begin{enumerate}
		\item
			The spectrum of $a$ is a non-empty and compact subset of $\cc$.
		\item
			Let $A$ be unital.
			Then the resolvent of $a$ is analytic.
			More precisely, for all $\lambda, \lambda_0 \in \cc \setminus \spa(a)$ with $\abs{\lambda - \lambda_0} < 1/\norm{\rslv_a(\lambda_0)}$, it holds that 
			\[
				\rslv_a(\lambda)
				=
				\sum_{n=0}^{\infty}
				\rslv_a(\lambda_0)^{n+1}(\lambda - \lambda_0)^n.
			\]
	\end{enumerate}
\end{theorem}
\begin{proof}
	We can for (i) also assume that $A$ is unital.
	In this case, we can use a ``shift by $a$''-map to see that $\sp(a)$ is closed:
	Namely, the map 
	\[
		h_a\mc \cc \to A,~
		\lambda \mapsto a - \lambda
	\]
	is continous, and so the preimage $h^{-1}(\units{A}) $ is open (we showed in \cref{banach:gou},\ref{banach:gou:is-open} that $\units{A}$ is open.)
	Now $\spa(a) = \cc \setminus h^{-1}(\units{A})$, and so it is indeed closed in $\cc$.
	Next, we show that $\spa(a)$ is bounded by $\norm{a}$:
	Let $\lambda \in \cc$ be a scalar with $\abs{\lambda} > \norm{a}$.
	Then $\norm{1 - a\lambda^{-1}}< 1$, and so it's invertible in $A$ (\cref{banach:gou},\ref{banach:gou:ps}).
	Now this implies that $\lambda - a = \lambda(1-a\lambda^{-1})$ is invertible in $A$ too, and so $\lambda \notin \spa(a)$; hence $\spa(a)$ is bounded and closed, so compact.\par
	Next, we show that the resolvent is analytic:
	Set $U \defined \cc \setminus \spa(a)$, and let $\lambda_0, \lambda \in U$ with $\abs{\lambda - \lambda_0} < 1/\norm{\rslv_a(\lambda_0)}$.
	Since the elements $b\defined a - \lambda$ and $b_0 \defined a - \lambda_0$ commute, we get from \cref{banach:gou},
	\[
		\rslv_a(\lambda)
		=
		b^{-1}
		= 
		\sum_{n=0}^{\infty}b_0^{-(n+1)}(b_0-b)^n
		=
		\sum_{n=0}^{\infty}\rslv_a(\lambda_0)^{n+1}
		(\lambda-\lambda_0)^n,
	\]
	and hence $\rslv_a$ is analytic around $\lambda_0$ in the disc of radius $1/\norm{\rslv_a(\lambda_0)}$.
	\par 
	To show that $\spa(a)$ is non-empty, assume to the contrary that it is.
	Then the resolvent function $\rslv_a$ is defined on all of $\cc$, and since $\rslv_a$ is analytic, we get from \cref{banach:liouville} that $\rslv_a$ is unbound.
	However, for all $\lambda \in \cc$ with $\abs{\lambda} > \norm{a}$, we have 
	\[
		\norm{\rslv_a(\lambda)}
		=
		\norm{(\lambda-a)^{-1}}
		=
		\abs{\lambda}^{-1}
		\cdot 
		\norm{(1-a/\lambda)^{-1}}
		\leq 
		\frac{\abs{\lambda^{-1}}}{1-\norm{a/\lambda}}
		=
		\frac{1}{\abs{\lambda} - \norm{a}},
	\]
	so $\rslv_a$ is bounded outside of the disc $D\defined \lset \lambda \in \cc \ssp \abs{\lambda} \leq \norm{a} + 1\rset$.
	But $D$ is compact, and the restriction of $\rslv_a$ to is continous, so $\restrict{\rslv_a}{D}$ is bounded too. 
	In total, we get that $\rslv_a \mc \cc \to A$ is bounded, contradicting our initial observation, and hence $\spa_a$ is non-empty.
\end{proof}

\begin{cor}[Gel'fand-Mazur]
	\label{banach:gm-theorem}
	Let $A$ be a unital Banach algebra over $\cc$.
	If $A$ is a field, then there is a canonical isometric isomorphism of algebras $\cc \isomorphism A$.
\end{cor}

\begin{proof}
	For every element $a\in A$, the spectrum $\spa(a)$ is non-empty.
	Since $A$ is a field, this impliesthat for every $a\in A$, there is a unique $\lambda(a) \in \cc$ such that $a = \lambda(a)$.
	The resulting map $\lambda \mc A \to \cc$ is linear and multiplicative; it's an isometry since 
	\[
		\norm{a}
		=
		\norm{\lambda(1)\cdot \une}
		=
		\abs{\lambda(a)}\cdot \norm{\une}
		=
		\abs{\lambda(a)}
	\]
	holds.
	In particular, the map $\lambda$ is injective too.
\end{proof}

\subsection{Spectral radius}

\begin{defn}
	Let $A$ be an algebra, and $a\in A$.
	The \emph{spectral radius} of $a$ is defined as $\srad(a) \defined \sup_{\lambda \in \spa(a)}\abs{\lambda}$.
	\glsadd{spectral-radius}
\end{defn}

\begin{prop}
	Let $A$ be a Banach algebra over $\cc$ and $a \in A$.
	Then $\srad(a) \leq \norm{a}$ holds and the sequence $(\norm{a^n}^{1/n})$ converges to the spectral radius of $a$:
	\[
		\srad(a)
		=
		\lim_{n\to \infty}
		\norm{a^n}^{1/n}.
	\]
\end{prop}

\begin{proof}
	\coms This is too much analysis for me :( \come
\end{proof}

\subsection{Commutative Banach algebras}

\begin{defn}
	Let $A$ be a Banach algebra.
	A \emph{character} on $A$ is a non-zero algebra homomorphism $A \to \cc$.
	The vector space of characters on $A$ is denoted by $\csp(A)$ and is called the \emph{spectrum} of $A$.
	\glsadd{spectrum-of-algebra}
\end{defn}

\begin{mlem}
	Let $X$ be a topological $\kk$-vector space and let $\pphi \mc X \to \kk$ be a linear map.
	Then $\pphi$ is continous if and only if its kernel is closed in $X$.\footnote{Slogan: \emph{Continuity can be checked at the generic fiber.} \coms I have no clue if this makes sense, but maybe \come}
\end{mlem}

\begin{lem}
	Let $A$ be a normed algebra, and $\idm \sse A$ a maximal ideal.
	Then $\idm$ is closed in $A$.
\end{lem}

\begin{prop}
	\label{banach:char-bij}
	Let $A$ be a unital and commutative Banach algebra.
	Then the assignement $\chi \mapsto \ker(\chi)$ induces a bijection
	\[
		\csp(A)
		\isomorphism
		\lset
		\text{maximal ideals of $A$}
		\rset.
	\]
\end{prop}

\begin{proof}
	The maximal ideals of $A$ are precisely given by the set of kernels of surjective ring maps $A \twoheadrightarrow k$, where $k$ is a field extension of $A$. 
	Now every such kernel $\idm$ is closed in $A$ (by the above lemma), and hence the quotient $A/\idm$ is a unital Banach algebra too.
	By Gel'fand-Mazur (\cref{banach:gm-theorem}), we have that $k = A/\idm \cong \cc$, so the set of maximal ideals of $A$ corresponds precisely to the set of characters on $A$.
\end{proof}

\begin{mrem}
	In particular, this implies that every character on $A$ is automatically continous, since its kernel is a maximal ideal, hence closed.
\end{mrem}

\begin{recall}
	Let $(X,\gnorm)$ be a normed space.
	\begin{enumerate}
		\item
			The \emph{weak topology} on $X$ is the weakest topology on $X$ such that all linear functionals $f\mc X \to \kk$ are continous.
			It is coarser than the topology induced by $\gnorm$, but still Hausdorff.
			For a sequence $(x_n)$ we say that it converges \emph{weakly} to a $x\in X$ if for all $f\in \dual{X}$, the sequence $f(x_n)$ converges to $x$ in $\kk$; in that case we write $x_n \rightharpoonup x$.
			It turns out that weak convergence is equivalent to convergence in the weak topology, i.e. a sequence $(x_n)$ in $X$ converges in the weak topology if and only if $x_n \rightharpoonup x$.

		\item
			The \emph{weak \startopology{}} on $\dual{X}$ is the weakest topology on $\dual{X}$ such that the maps 
			\[
				\dual{X} 
				\to
				\cc,
				~
				\pphi 
				\mapsto 
				\pphi(x)
			\]
			are continous for all $x\in X$.
			It is coarser than the weak topology on $X$, but again still Hausdorff.
			For a sequence $(f_n)$ in $\dual{X}$ and a $f\in \dual{X}$, we have that $(f_n)$ converges to $f$ in the weak \startopology{} on $\dual{X}$ if and only if $f_n(x)$ converges to $f(x)$ in the $\gnorm$-induced topology on $X$.
			If $X$ is a reflexife Banach space, then the weak topology and the weak \startopology{} on $X$ agree.
	\end{enumerate}
\end{recall}

\begin{defn}
	Let $A$ be a commutative unital Banach algebra.
	Then as a topological space, the specturm $\csp(A)$ of $A$ is endowed with the subspace topology of the weak \startopology.
\end{defn}

\begin{prop}
	Let $A$ be a commutative unital Banach algebra.
	\begin{enumerate}
		\item
			For every $a\in A$, it holds that 
			\[
				\spa(a)
				=
				\lset 
				\chi(x)
				\ssp
				\chi \in \csp(A)
				\rset.
			\]
		\item
			The topologial space $\csp(A)$ is compact.
	\end{enumerate}
\end{prop}

\begin{proof}
	\leavevmode
	\begin{enumerate}
		\item
			Let $\lambda \in \spa(a)$.
			Then $a-\lambda$ is not invertible in $A$, and hence contained in a maximal ideal $\idm$.
			By the bijection from \cref{banach:char-bij}, there is a character $\chi \mc A \to \cc$ with kernel $\idm$.
			In particular, $\chi(a-\lambda) 0$, and since $\chi$ is $\cc$-linear, this implies $\chi(a) = \lambda$.
			\par
			Converseley, for every $\chi \in \csp(A)$, it holds that $x-\chi(a) \in \ker(\chi)$, and since $\ker(\chi)$ is a maximal ideal, $a-\chi(a) \notin \units{A}$ follows.  
		\item

	\end{enumerate}
\end{proof}

\begin{defn}
	Let $A$ be a unital and commutative Banach algebra and $a\in A$ an element. 
	Then the map evaluation map 
	\[
		\csp(A)
		\to
		\cc
		,~
		\chi
		\mapsto 
		\chi(a)
	\]
	induce a well-defined map 
	\[
		G_A\mc 
		A
		\to 
		\contc(\csp(A))
	\]
	which is called the \emph{Gel'fand transformation} of $A$.
	\glsadd{gelfand-trafo}
\end{defn}
