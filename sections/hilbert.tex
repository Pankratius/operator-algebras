\section{Recollection on Hilbert spaces}

\subsection{Scalar products}

\begin{defn}
	Let $E, F, G$ be $\cc$-vector spaces.
	\begin{enumerate}
		\item 
			A map of sets $f\mc E \to G$ is \emph{antilinear} if for all $x,y\in E$ and $\lambda \in \cc$,
			\[
				f(x + \lambda y) 
				=
				f(x) + \overline{\lambda}f(y) 
			\]
			holds.
		\item
			A map of sets $B\mc E \times F \to G$ is \emph{sesquilinear} if the map 
			\[
				B(-,y)
				\mc
				E \to G,~
				x\mapsto B(x,y)
			\]
			is $\cc$-linear and for all $y \in F$ and the map
			\[
				B(x,-)
				\mc
				F \to G,~
				y \mapsto B(x,y)
			\]
			is $\cc$-antilinear.
		\item 
			A \emph{sesquilinear form} on $E$ is a sesquilinear map $E \times E \to \cc$.

	\end{enumerate}
\end{defn}

\begin{defn}
	Let $E, F$ be $\kk$-vector spaces.
	A map $B\mc E \times E \to F$ is \emph{symmetric} if for all $x,y\in E$, it holds that $B(x,y) = B(y,x)$.
\end{defn}

\begin{mrem}
	Any $\cc$-vector space is also an $\rr$-vector space, via restriction of scalars along the natural map $\rr \hookrightarrow \cc$.
	If $E$ is a $\cc$-vector space, let us write for now $E_{\rr}$ for the induced $\rr$-vector space.
	For any antilinear map of $\cc$-vector spaces $f\mc E \to G$, the induced map $E_{\rr} \to F_{\rr}$ of $\rr$-vector spaces is $\rr$-linear.
	The same holds for sesquilinear maps, i.e. if $B\mc E \times F \to G$ is a sesquilinear map of $\cc$-vector spaces, then the induced map $E_{\rr} \times F_{\rr} \to G_{\rr}$ is $\rr$-bilinear.
	\glsadd{restriction}
\end{mrem}

\begin{lem}[Polarization identities]
	\leavevmode
	\begin{enumerate}
		\item 
			Let $E,F$ be $\kk$-vector spaces and let $B\mc E \times E \to F$ be a bilinear map.
			Then, for all $x,y\in E$, it holds that
			\begin{equation}
				\label{pol-bil}
				2\left(B(x,y)+B(y,x)\right)
				=
				B(x+y,x+y) - B(x-y,x-y).
			\end{equation}
		\item
			Let $E,F$ be $\cc$-vector spaces and let $B\mc E \times E \to F$ be a sesquilinear map\footnote{Note that we do not require $B$ to be hermitian}.
			Then, for all $x,y \in E$, it holds that 
			\begin{equation}
				\label{pol-sesq}
				\begin{split}
				4\left(B(x,y)\right)
				&=
				B(x+y,x+y)
				-
				B(x-y,x-y)
				\\
				&
				+
				iB(x+iy,x+iy)
				-
				iB(x-iy,x-iy).
				\end{split}
			\end{equation}
	\end{enumerate}
\end{lem}

\begin{proof}
	\leavevmode
	\begin{enumerate}
		\item
			By bilinearity of $B$, we have 
			\begin{align*}
				B(x+y,x+y) 
				&
				=
				B(x,x)
				+
				B(x,y)
				+
				B(y,x)
				+
				B(y,y)
				\intertext{as well as}
				B(x-y,x-y)
				&
				=
				B(x,x)
				-
				B(x,y)
				-
				B(y,x)
				+
				B(y,y).
			\end{align*}
			Subtracting the second equation from the first gives the desired identity.
		\item
			...
	\end{enumerate}
\end{proof}

\begin{cor}
	\label{hilbert:sesq-on-diag}
	Every sesquilinear/symmetric bilinear map $E \times E \to F$ is determined by its value on the diagonal.
\end{cor}

\begin{cor}
	\label{hil:herm-equiv}
	Let $E$ be a $\cc$-vector space and $B\mc E \times E \to \cc$ a sesquilinear form. 
	Then the following are equivalent:
	\begin{enumerate}
		\item 
			For all $x,y\in E$, it holds that $B(x,y) = \overline{B(y,x)}$.
		\item
			For all $x \in E$, it holds that $B(x,x) \in \rr$.
	\end{enumerate}
\end{cor}

\begin{proof}
	For the implication (i) $\Rightarrow$ (ii), we have that (i) implies that for every $x\in E$, it holds that $B(x,x) = \overline{B(x,x)}$, and hence $B(x,x) \in \rr$ follows.
	To get (ii) $\Rightarrow$ (i), we first note that the map $B'\mc (x,y) \mapsto \overline{B(y,x)}$ is still a sesquilinear form on $E$, and hence $S\defined B-B'$ is too.
	We need to show that $S$ vanished on $E$.
	But since $S$ vanishes on the diagonal by assumption, this is follows from \cref{hilbert:sesq-on-diag}.
\end{proof}

\begin{defn}
	\leavevmode
	\begin{enumerate}
		\item
			Let $E$ be a $\cc$-vector space.
			A \emph{hermitian form} on $E$ is a sesquilinear form $B$ on $E$ that satisfies the equivalent conditions of \cref{hil:herm-equiv}. 
			A hermitian form $B$ is called \emph{positive} if $B(x,x) \geq 0$ holds for all $x\in E$.
		\item
			Similiarly, let $F$ be a $\rr$-vector space.
			A form $B$ on $F$ is called positive if $B(x,x) \geq 0$ holds for all $x\in F$.
		\item
			Let $G$ be a $\kk$-vector space.
			A form $B$ on $G$ is called \emph{definite} if $B(x,x) = 0$ holds if and only if $x = 0$.
	\end{enumerate}
\end{defn}

\begin{defn}
	Let $E$ be either a $\kk$-vector space.
	A form $B$ on $E$ that is either symmetric bilinear and positive or hermitian sesquilinear and positive, respectively, is called a \emph{scalar semi-product} on $E$, and the tuple $(E,B)$ is called a \emph{pre-Hilbert space}.
	We often denote the scalar semi-product $B$ as $(x,y) \mapsto \innp{x}{y}$.
	If $B$ is additionally definite, then $B$ is called a \emph{scalar product}.
\end{defn}

\begin{hwarning}
	Usually, people require the scalar semi-product in the definition of a pre-Hilbert space to be definite, i.e. ``A hilbert space is a $\kk$-vector space together with a scalar product''.
	We will not make this assumption in the definition of a pre-Hilbert space.
\end{hwarning}

\begin{lem}[Cauchy-Schwarz]
	\label{hilb:cs-stat}
	Let $E$ be a pre-Hilbert space.
	Consider the map 
	\[
		\gnorm	
		\mc 
		E
		\to
		\rr
		,~
		x
		\mapsto 
		\innp{x}{x}^{1/2}.
	\]
	This map is well-defined, and for all $x,y\in E$, it holds that 
	\begin{equation}
		\label{hil:cs-eq}
		\tag{CS-I}
		\abs{\innp{x}{y}}
		\leq 
		\norm{x}\cdot \norm{y}.
	\end{equation}
\end{lem}

\begin{proof}
	If either one of $x,y$ is zero, then both sides of the inequality vanish and there is nothing more to show.
	So assume that $x,y\neq 0$ and set $\mu \defined \innp{x}{y}/\abs{\innp{x}{y}}$.
	For every $t \in \rr$ we have, by the polarisation identities,
	\[
		0
		\leq
		\norm{x - \mu t y}^2
		=
		\norm{x}^2
		-
		2t
		\innp{x}{y}
		+
		t^2 \norm{y}^2
		.
	\]
	This is a real, quadratic polynomial in $t$ that is strictly positive, so it can have at most one real root.
	The determinant identity now gives $\abs{\innp{x}{y}}^2 \leq \norm{x}^2 \cdot \norm{y}^2$, and since all the terms involved are non-negative, the result follows.
\end{proof}

\begin{cor}
	Let $(E,\ginnp$)be a pre-Hilbert space over $\kk$.
	Then the map $x \mapsto \norm{x}$ from \cref{hilb:cs-stat} is a \emph{semi-norm} on $E$, i.e. it has the following two properties: 
	\begin{enumerate}
		\item
			For all $x\in E$ and $\lambda \in \kk$, it holds that $\norm{\lambda x} = \abs{\lambda} \norm{x}$.
		\item
			For all $x,y\in E$, it holds that $\norm{x+y}\leq \norm{x} + \norm{y}$.
	\end{enumerate}
\end{cor}

\begin{mrem}
	\leavevmode
	\begin{enumerate}
		\item
			If $(E,\gnorm)$ is a pre-Hilbert space, then the map 
			\[
				d_{\gnorm}\mc E\times E \to \kk,~
				(x,y) \mapsto \norm{x-y}
			\]
			is a \emph{pseudo-metric} on $E$, i.e. satisfies the following three properties:
			\begin{enumerate}
				\item 
					For all $x\in E$, it holds that $d_{\gnorm}(x,x) = 0$.
				\item
					For all $x,y\in E$, it holds that $d_{\gnorm}(x,y) = d_{\gnorm}(y,x)$.
				\item
					For all $x,y,z$ in $E$, $d_{\gnorm}$ satisfies the \emph{triangle inequality}, i.e. it holds that
					\[
						d_{\gnorm}(x,z)
						\leq 
						d_{\gnorm}(x,y)
						+
						d_{\gnorm}(y,z).
					\]
			\end{enumerate}
			This pseudo-metric is a metric if and only if $\gnorm$ is non-degenarate.
			\item 
				The pseudo-metric $d_{\gnorm}$ induces a topology on $E$, with a basis of opens given by the collection 
				\[
					B_r
					\defined 
					\lset 
					y\in E
					\ssp 
					d_{\gnorm}(x,y) < r
					\rset
				\]
				with $r \in \rr_{>0}$ and $x\in E$.
				This topology is called the \emph{pseudo-metric topology} on $E$ generated by $d_{\gnorm}.$
				It is Hausdorff if and only if $\gnorm$ is non-degenarate.
	\end{enumerate}
\end{mrem}

\begin{mdefn}
	Let $(E,\gnorm_E)$ and $(F,\gnorm_F)$ be pre-Hilbert spaces over $\kk$, and $f\mc E \to F$ a $\kk$-linear map.
	We say that $f$ is \emph{bounded} if there is a $M \in \rr_{>0}$ with $\norm{f(x)}_F \leq M \norm{x}_E$ for all $x\in E$.
	In that case, the scalar 
	\[
		p(f) \defined \inf \lset M \in \rr_{>0} \ssp \norm{f(x)}_F \leq M \norm{x}_E \rset
	\]
	is called the \emph{operator norm} of $f$.
\end{mdefn}

\begin{mlem}
	Let $f\mc E \to F$ be a linear map between pre-Hilbert spaces.
	Then $f$ is continous if and only if $f$ is bounded.
\end{mlem}
\begin{mdefn}
	Let $E,F$ be pre-Hilbert spaces.
	We denote by $\cl(E,F)$ the $\kk$-vector space of continous $\kk$-linear maps $E \to F$.
\end{mdefn}

\begin{mlem}
	Assume we are in the above setup.
	Then the operator norm gives $\cl(E,F)$ the structure of a pre-Hilbert space.
\end{mlem}

\begin{prop}
	\label{hilbert:map-to-dual}
	Let $(E,\gnorm)$ be a pre-Hilbert space.
	For every $y\in E$, the map 
	\[
		f_y\mc 
		E \to \kk,~
		x \mapsto \innp{x}{y}
	\]
	is $\kk$-linear and continous.
	This gives a well-defined assignment 
	\[
		\pphi \mc 
		E
		\to 
		\cl(E,\kk)
		,~
		y
		\mapsto 
		f_y
	\]
	which is anti-linear.
	\coms If $\gnorm$ is non-degenarate\come, then this is an isometry.
\end{prop}

\begin{proof}
	By Cauchy-Schwarz, we have 
	\[
		\abs{f_y(x)}
		=
		\abs{\innp{x}{y}}
		\leq 
		\norm{y}
		\cdot
		\norm{x},
	\]
	i.e. $f_y$ is bounded by $\norm{y}$.
	Hence $f_y \in \cl(E,\cc)$ and $\norm{f_y} \leq \norm{y}$.
	By the definition of the operator norm, we also have 
	\[
		\norm{y}^2 
		= 
		f_y(y)
		\leq 
		\norm{f_y} \cdot \norm{y}
		,
	\]
	and hence $\norm{y} \leq \norm{p_y}$.\footnote{Here we need that $\gnorm$ is non-degenarate. Otherwise, the proof should only work if we restrict to a complement of the radical of $E$ \coms (?) \come.} 
	So $\pphi$ is an isometry.
\end{proof}

\begin{defn}
	Let $E$ be a pre-Hilbert space.
	\begin{enumerate}
		\item 
			Let $x,y\in E$
			We say that $x$ and $y$ are \emph{orthogonal} if $\innp{x}{y} = 0$.
		\item 
			Let $A \sse E$ be a subset.
			The \emph{orthogonal complement} of $A$ is defined as 
			\[
				\orco{A}
				\defined 
				\lset 
				x\in E
				\ssp 
				\innp{x}{a}
				=
				0
				\text{ for all $a\in A$}
				\rset.
			\]
			\glsadd{orthogonal-complement}
	\end{enumerate}
\end{defn}

\begin{lem}
	\label{hilbert:comp-is-subspace}
	Let $E$ be a pre-Hilbert space.
	Then for every subset $A\sse E$, its orthogonal complement $\orco{A}$ is a closed $\kk$-linear subspace.
\end{lem}
\begin{proof}
	We have 
	\[
		\orco{A}
		=
		\bigcap_{y\in A}
		\ker(f_y),
	\]
	and since each of the $f_y$ is a $\kk$-linear map, this intersection is a $\kk$-linear subspace of $E$.
	Furthermore, each of the $\ker(f_y)$ is closed in $A$, as it is the preimage of the closed set $\lset 0 \rset$ under the continous map $f_y$.
	Since arbitrary intersections of closed sets are again closed, it follows that $\orco{A} \sse E$ is closed too.
\end{proof}

\subsection{Hilbert spaces}

\begin{defn}
	A \emph{Hilbert space} is a pre-Hilbert space $(E,\ginnp)$ that is separated, and complete with respect to $\ginnp$.
\end{defn}

\begin{mlem}
	Let $(E,\ginnp)$ be a pre-Hilbert space.
	\begin{enumerate}
		\item
			Write $\nsep(E) \defined \orco{\lset 0 \rset}$.
			Then $\ginnp$ descends to a well-defined scalar semi-linear product $\ginnp'$ on $E/\nsep(E)$, which is additionally non-degenerate (i.e. $E/\nsep(E)$ is separated).
		\item
			Denote by $\hilbf{E}\defined \left(E/\nsep(E)\right)^{\what{~}}$ the completion of $E/\nsep(E)$ with respect to $\ginnp'$.
			Then $\hilbf{E}$ is a Hilbert space.
			It's universal in the sense that for any other Hilbert space $(F,\ginnp_F)$ and every continous map $E\to F$, there is a continous linear map $\hilbf{E} \to F$ such that the following diagram commutes:
			\[
				\begin{tikzcd}
					E
					\ar{r}
					\ar{d}
					&
					F
					\\
					\hilbf{E}
					\ar[dashed]{ur}[below right]{\exists !}
					&
				\end{tikzcd}
			\]

	\end{enumerate}
\end{mlem}

\subsection{Projection formular}

\begin{prop}
	\label{hilbert:proj-form}
	Let $H$ be a Hilbert space, and $C\sse H$ a convex and closed subset of $H$.
	For every $x\in H$, there is a a unique $x_0 \in C$ such that $\norm{x-x_0} \leq \norm{x-c}$ holds for all $c\in C$.
\end{prop}


\begin{proof}
	Let 
	\[	
		d \defined \inf \lset \norm{x-y} \ssp y\in C\rset.
	\]
	Then there is a sequence $(y_n)$ of elements $y_n \in C$ such that 
	\[d \leq \norm{x-y_n} \leq d + 1/n\] for all $n \geq 1$.
	We are going to show that this sequence is a Cauchy sequence.
	By completeness of $H$, this then implies that $(y_n)$ converges, and since $C$ is closed its limit will be the desired point $x_0$.
	\par
	Let $y,z \in C$ and set $b\defined x - 1/2(y+z)$ and $z\defined 1/2(y-z)$.
	Since $C$ is convex, we have $1/2(y+z)\in C$ and hence $\norm{b} \geq d$.
	We then have 
	\begin{align*}
		\norm{x-y}^2
		+
		\norm{x-z}^2
		&=
		\norm{b-c}^2
		+
		\norm{b+c}^2
		\\
		&=
		2
		\left(\norm{b}^2 + \norm{c}^2\right)
		\\
		&
		\geq 
		2d^2 
		+
		\frac{1}{2} \norm{y-z}^2.
		\intertext{Rearranging gives}
		\norm{y-z}^2
		&
		\leq 
		2 \left(\norm{x-y}^2 - d^2\right)
		+
		2\left(\norm{x-y}^2 - d^2\right).
		\intertext{In particular, we get for elements $y_n, y_m$ of the seequence $(y_n)$:}
		\norm{y_n - y_m}
		&
		\leq
		2
		\left(\left(d+\frac{1}{n}\right)^2 - d^2\right)
		+
		2
		\left(\left(d+\frac{1}{m}\right) - d^2\right),
	\end{align*}
	which converges to zero as $n,m\to \infty$.

\end{proof}

\begin{prop}
	Let $H$ be a Hilbert space and let $E \sse H$ be a closed subspace.
	Then it holds that $H = E \oplus \orco{E}$.
\end{prop}

\begin{proof}
	If $x\in E \cap \orco{E}$, then $\innp{x}{x} = 0$, and since $\ginnp$ is non-degenerate, $x = 0$ follows.
	Let $x \in H$ be any point of $H$.
	Since $E$ is a closed subspace, we can apply \cref{hilbert:proj-form} and get a unique $y_0 \in E$ that minimizes the function $E \to \kk,~ y \mapsto \norm{y-x}$.
	Now $y_0 + \lambda y \in E$ holds for every $y \in E$ and $\lambda \in \kk$.
	...	
\end{proof}

\begin{construction}
	Let $E \sse H$ be a closed subspace of a Hilbert space $H$.
	The orthogonal decomposition $H = E \oplus \orco{E}$ induces a canonical projection $p\mc H \twoheadrightarrow E$, which we call the \emph{orthogonal projection onto $E$}.
	Let now $v \in H$ be any element with decomposition $v = x + y$ for $x\in E, y \in \orco{E}$; since $x$ and $y$ are orthogonal to each other, we have 
	\[
		\norm{v}^2
		=
		\norm{x + y}^2
		=
		\norm{x}^2 + \norm{y}^2
		\geq 
		\norm{x}^2
		=
		\norm{P(v)}^2,
	\]
	and hence $P$ is continous with $\norm{P} \leq 1$.
\end{construction}


\begin{cor}
	Let $H$ be a Hilbert space and $A \sse H$ a subset.
	Then $\orcod{A}$ is the smallest closed subvector space of $H$ that contains $A$.
	In particular, if $A$ is a subvector space of $H$, then $\orcod{A} = \overline{A}$.
\end{cor}

\begin{proof}
	By \cref{hilbert:comp-is-subspace}, we have that $\orcod{A}$ is indeed a closed subvector space of $H$.
	Furthermore, $A \sse \orcod{A}$ holds, as every element of $A$ is orthogonal to $\orco{A}$.
	So $\orcod{A}$ is a closed subvector space that contains $A$.
	Let now $E$ be an inclusion-minimal subvector space that is closed and contains $A$ (we can use Zorn's Lemma to deduce it's existence: the collection of all closed subvector spaces that contain $A$ is non-empty, since $H$ satisfies all three properties; every chain of closed subvector spaces containing $A$ has an inclusion-minimal element given by the intersection over the chain --- this intersection is again closed because arbitrary intersections of closed are, the same holds for intersections of subvector spaces and subsets that contain $A$).
	By previous paragraph, we have $E \sse \orcod{A}$.
	Applying the decomposition result, we can write every $x\in \orcod{A}$ as $x = y + z$, with $y\in E$ and $z \in \orco{E}$.
	Since $A \sse E$ it follows that $\orco{E} \sse \orco{A}$ (taking the orthogonal complement is in general inclusion-reversing on subsets of a Hilbert space), and in particular, it suffices to show that $z = 0$.
	But $ z = x -y \in \orcod{A}$ (for that, we use $E \sse \orcod{A}$), so $z \in \orcod{A} \cap \orco{A} = 0$. 
\end{proof}

\begin{prop}[Riesz-Fréchet]
	\label{hilbert:rz-theorem}
	The map
	\[
		\pphi
		\mc 
		H 
		\to 
		\cl(H, \kk),
		~
		y 
		\mapsto
		f_y \defined \innp{-}{y},
	\]
	from \cref{hilbert:map-to-dual} is bijective.
\end{prop}

\begin{proof}
	By the definition of a Hilbert space $\gnorm$ is non-degenerate, so \cref{hilbert:map-to-dual} gives that $\pphi$ is an anti-linear isometry.
	We have that $\pphi$ is injective, by the non-degeneracy of $\ginnp$ --- indeed, we have $\ker \pphi = \orco{H} = 0$, and every anti-linear map with trivial kernel is automatically injective (because we only need the additivity to deduce it).
	\par
	So we're left with showing that $\pphi$ is surjective.
	For that, let $g\mc H \to \kk$ be a bounded non-zero functional on $H$.
	Set $E \defined \ker(g)$.
	As $g$ is non-zero, it is surjective onto $\kk$, so we have $H/E \cong \kk$.
	Now the decomposition theorem gives $H = E \oplus \orco{E}$, and hence we get that $\orco{E}$ is a subspace of codimension one.
	Choose a generator $v_0$ of $\orco{E}$; without loosing generality we can assume $\norm{v_0} = 1$.
	We claim that for this generator and $\lambda \defined \overline{g(v_0)}$, it holds that $g = \pphi(\lambda \cdot v_0) = \lambda \cdot \innp{-}{v_0}$:
	Indeed, decompose any $z\in H$ as $z = x + \mu v_0$ for $\mu\in \kk$ and $x\in E = \ker(g)$.
	Then $g(z) = \mu \cdot g(v_0)$, and 
	\begin{align*}
		\innp{z}{v_0}
		&=
		\innp{x + \mu v_0}{v_0}
		\\
		&=
		\innp{x}{v_0} + \mu \innp{v_0}{v_0} = \mu, 
	\end{align*}
	where we used $x \in \ker(g) = \left(\orco{E}\right)^{\perp}$ to get from the second to the third line.
	So $\lambda \cdot \innp{-}{v} = g$, and we're done.
\end{proof}

\begin{cor}
	Let $H$ be a Hilbert space.
	\begin{enumerate}
	\item
	The bounded-dual space $\dual{H}$ is a Hilbert space too, via the scalar product
	\[
		\innp{f}{g}_{\dual{H}}
		\defined 
		\innp{\psi(g)}{\psi(f)}.
	\]
	Here, $\psi\mc \dual{H} \isomorphism H$ is the inverse of the Riesz isomorphism $\pphi$ (\cref{hilbert:rz-theorem}).
	\item 
	This scalar product is compatible with the operator norm, i.e. for every $f\in \dual{H}$, it holds that $\innp{f}{f}_{\dual{H}} = \norm{f}^{\mathrm{op}}$.
	\item
	The Riesz isomorphism $\pphi$ is a homeomorphism of topological spaces.
	\end{enumerate}
\end{cor}

\begin{mrem}
	One might wonder what sort of naturality is satisfied by the Riesz isomorphism.
	A reasonable guess would be that it commutes with taking the dual map, i.e. that for every continous linear map of Hilbert spaces $f\mc E \to F$, the diagram 
	\[
		\begin{tikzcd}
		E
		\ar{r}[above]{f}
		\ar{d}[left]{\cong}
		&
		F
		\ar{d}[right]{\cong}
		\\
		\dual{E}
		&
		\dual{F}
		\ar{l}[below]{\dual{f}}
		\end{tikzcd}
	\]
	commutes.
	However, this is satisfied if and only if $f$ is an isometry! 
	For every $x\in E$, we have $\dual{f}\left(\phi_Y(f(x))\right) = \innp{f(-)}{f(x)}$, which is the same map as $\innp{-}{x}$ iff $f$ is an isometry for every $x\in E$.
\end{mrem}

\subsection{The adjoint of a continous linear map}

\begin{prop}
	Let $E,F$ be Hilbert spaces and $f\mc E\to F$ a continous linear map.
	Then there exists a unique continous linear map $\adj{f}\mc F \to F$, such that for all $x\in E$ and $y\in F$, it holds that $\innp{f(x)}{y}_F = \innp{x}{\adj{f}(y)}_E$.
\end{prop}

\begin{proof}
	We obtain this map via the Riesz isomorphism --- for every $y\in F$, we have the continous linear map
	\[
		\innp{f(-)}{y}
		\mc 
		E
		\to 
		\kk,
		z
		\mapsto 
		\innp{f(z)}{y},
	\]
	so there a unique $x \eqqcolon \adj{f}(y)$ in $E$ such that $\innp{-}{x} = \innp{f(-)}{y}$.
	One then checks that the map $y \mapsto \adj{f}(y)$ is linear.
\end{proof}

\begin{prop}
	\leavevmode
	\begin{enumerate}
		\item
		Let $E,F$ be Hilbert spaces.
		Then the map 
		\[
			\cl(E,F)
			\to 
			\cl(F,E)
			,~
			f
			\mapsto 
			\adj{f}
		\]
		is a linear isometry.
		Furthermore, for every $f\in \cl{E,F}$, it holds that $\adjd{f} = f$ and $\norm{\adj{f} \circ f } = \norm{f}^2$.
		\item

			For every other Hilbert space $H$ and $g \in \cl{F,H}$ it holds that $\adj{\left(g\circ f\right)} = \adj{f} \circ \adj{g}$.
	\end{enumerate}	
\end{prop}

\begin{prop}
	Let $E,F$ be Hilbert spaces and $f \in \cl{E,F}$.
	Then there are decompositions 
	\begin{align*}
		E
		&
		=
		\im(\adj{f})
		\oplus 
		\orco{\ker(f)}
		\\
		F
		&
		=
		\ker(\adj{f})
		\oplus 
		\orco{\im(f)}.
	\end{align*}
\end{prop}

\begin{defn}
	\leavevmode
	\begin{enumerate}
		\item
			Let $E,F$ be Hilbert spaces and $f\in \cl{E,F}$.
			Then $f$ is \emph{unitary} if $\adj{f} \circ f = \id_E$ and $f\circ \adj{f} = \id_F$ holds.
		\item
			Let $E$ be a Hilbet space and $f\in \cl{E,E}$.
			\begin{enumerate}
				\item
					We say that $f$ is \emph{normal} if $\adj{f} \circ f = f \circ \adj{f}$ holds.
				\item
					We say that $f$ is \emph{self-adjoint} if $\adj{f} = f$ holds.
				\item
					We say that $f$ is \emph{positive} if $f$ is self-adjoint and $\innp{f(x)}{x} \in \rr_{\geq 0}$ holds for all $x \in E$.
			\end{enumerate}
	\end{enumerate}
\end{defn}

\begin{lem}
	Let $E,F$ be Hilbert spaces and $f\in \cl(E,F)$.
	Then the following are equivalent:
	\begin{enumerate}
		\item
			$f$ is unitary.
		\item
			$f$ is surjective and $\adj{f} \circ f = \id_E$ holds.
		\item
			$f$ is a surjective isometry.
	\end{enumerate}
\end{lem}
